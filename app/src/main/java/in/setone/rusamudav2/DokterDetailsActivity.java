package in.setone.rusamudav2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DokterDetailsActivity extends AppCompatActivity {

    TextView tvNama,tvSpesialis,tvJadwal;
    ImageView imgBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dokter_details);

        Intent intent = getIntent();
        String[] data = intent.getStringArrayExtra("KEY_OF_DATA");

        tvNama = (TextView) findViewById(R.id.tvNamaDokter);
        tvSpesialis = (TextView) findViewById(R.id.tvSpesialis);
        tvJadwal = (TextView) findViewById(R.id.tvJadwal);

        tvNama.setText(data[0]);
        tvSpesialis.setText("Spesialis : "+data[1]);
        tvJadwal.setText("Jadwal Praktek : "+data[2]);

        imgBack = (ImageView) findViewById(R.id.imgBacks);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
