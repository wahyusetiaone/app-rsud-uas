package in.setone.rusamudav2.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.*;
import in.setone.rusamudav2.DokterDetailsActivity;
import in.setone.rusamudav2.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DokterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DokterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DokterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DokterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DokterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DokterFragment newInstance(String param1, String param2) {
        DokterFragment fragment = new DokterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_dokter, container, false);
        ListView listview =(ListView) rootView.findViewById(R.id.listDokter);
        final ArrayList<String[]> items = new ArrayList<>();
        items.add(0,new String[]{"Wahyu Setiawan","Special","Tanggal Praktek"});
        items.add(1,new String[]{"Melia jubir","Special","Tanggal Praktek"});
        items.add(2,new String[]{"Afina Sabana","Special","Tanggal Praktek"});
        items.add(3,new String[]{"Wahyu Setiawan","Special","Tanggal Praktek"});
        items.add(4,new String[]{"Melia jubir","Special","Tanggal Praktek"});
        items.add(5,new String[]{"Afina Sabana","Special","Tanggal Praktek"});
        items.add(6,new String[]{"Wahyu Setiawan","Special","Tanggal Praktek"});
        items.add(7,new String[]{"Melia jubir","Special","Tanggal Praktek"});
        items.add(8,new String[]{"Afina Sabana","Special","Tanggal Praktek"});
        ArrayAdapter<String[]> adapter = new ArrayAdapter<String[]>(getActivity(), R.layout.item_list_dokter,items){
            @Override
            public View getView(int position, View view, ViewGroup parent){
                LayoutInflater layoutInflater = getActivity().getLayoutInflater();
                View itemView = layoutInflater.inflate(R.layout.item_list_dokter,null,true);
                TextView tvNamaDokter,tvSpDokter;

                tvNamaDokter = itemView.findViewById(R.id.nama_dokter);
                tvSpDokter = itemView.findViewById(R.id.sp_dokter);

                tvNamaDokter.setText(items.get(position)[0]);
                tvSpDokter.setText(items.get(position)[1]);

                return itemView;
            }
        };
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent (getActivity(), DokterDetailsActivity.class);
                intent.putExtra("KEY_OF_DATA",items.get(i));
                startActivity(intent);
            }
        });
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
