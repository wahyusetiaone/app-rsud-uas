package in.setone.rusamudav2;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import in.setone.rusamudav2.fragment.*;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener, AboutFragment.OnFragmentInteractionListener, DokterFragment.OnFragmentInteractionListener, LayananFragment.OnFragmentInteractionListener, FasilitasFragment.OnFragmentInteractionListener {

    private TextView mTextMessage;
    // Memulai transaksi
    FragmentTransaction ft;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new HomeFragment());
                    ft.commit();
                    return true;
                case R.id.navigation_fasilitas:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new FasilitasFragment());
                    ft.commit();
                    Log.wtf("FASILITAS", "OK");
                    return true;
                case R.id.navigation_dokter:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new DokterFragment());
                    ft.commit();
                    Log.wtf("DOKTER", "OK");
                    return true;
                case R.id.navigation_layanan:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new LayananFragment());
                    ft.commit();
                    Log.wtf("LAYANAN", "OK");
                    return true;
                case R.id.navigation_about:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, new AboutFragment());
                    ft.commit();
                    Log.wtf("ABOUT", "OK");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        uri.getHost();
    }
}
